
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiProvider } from '../../provider/api';
import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from "../../../../shared/services/auth.service";
import { Injectable } from '@angular/core';
import { getLocaleDateFormat } from '@angular/common';
import { callbackify } from 'util';
import { AccountApi } from '../../provider/accounts';


@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class overviewComponent implements OnInit{
  
  playerData; user;
  accounts:AccountApi;


  constructor(
    public http:HttpClient,
    public authService: AuthService,
    public router: Router,
    public ngZone: NgZone,
  ){
   this.accounts = new AccountApi(http);
  }



  ngOnInit(){

    this.user = localStorage.getItem('user');
    this.user = JSON.parse(this.user);
   
    console.log("useruid: "+this.user.uid);
  this.loadData();

    
  }

  loadData() {


    this.accounts.getAccountData(this.user.uid, (error, result) => {


        this.playerData = result;
        console.log("log_- "+this.playerData.uid)
    
    
    });
  }
  

}
