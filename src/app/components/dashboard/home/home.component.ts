import { Component,OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiProvider } from '../provider/api';

import { Injectable } from '@angular/core';
import { getLocaleDateFormat } from '@angular/common';
import { callbackify } from 'util';
import { AccountApi } from '../provider/accounts';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{
  
  playerData;
  accounts:AccountApi;


  constructor(
    public http:HttpClient
  ){
   this.accounts = new AccountApi(http);
  }


  ngOnInit(){

    this.accounts.getAccountData((res) => {
 
      this.playerData = res;
      console.log("log_- "+this.playerData)


    });
      }


}
