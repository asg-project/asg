import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ApiProvider } from './api';



@Injectable()
export class AccountApi {
  
 
 constructor(public http: HttpClient) {}

 api: ApiProvider = new ApiProvider(this.http);



getAccountData(id, callback){
  this.http.get(this.api.dataURL+'/account/start?idGoogle='+id, this.api.httpOptions).subscribe(
    (response) => callback(null, response),
    (error) => callback(error.error.message)
);
}


}

