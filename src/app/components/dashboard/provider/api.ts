import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable()
export class ApiProvider {
 public dataURL: string = "http://localhost:8080";
 constructor(public http: HttpClient) {}

 public httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept' : 'application/json',
    '*':'*'
  })
}

}