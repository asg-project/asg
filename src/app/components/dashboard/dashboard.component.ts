import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from "../../shared/services/auth.service";
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { ApiProvider } from './provider/api';

import { Injectable } from '@angular/core';
import { getLocaleDateFormat } from '@angular/common';
import { callbackify } from 'util';
import { AccountApi } from './provider/accounts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit{
  
  playerData;
  user;
  accounts:AccountApi;
  error;

  constructor(
    public authService: AuthService,
    public router: Router,
    public ngZone: NgZone,
    public http:HttpClient
  ){
   this.accounts = new AccountApi(http);
  }


  ngOnInit(){

    this.user = localStorage.getItem('user');
    this.user = JSON.parse(this.user);
   
    console.log("useruid: "+this.user.uid);
  this.loadData();

    
  }

  loadData() {


    this.accounts.getAccountData(this.user.uid, (error, result) => {


        this.playerData = result;
        console.log("log_- "+this.playerData.uid)
    
    
    });
  }
  

}
