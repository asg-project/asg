import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignInComponent } from '../../components/sign-in/sign-in.component';
import { SignUpComponent } from '../../components/sign-up/sign-up.component';
import { DashboardComponent } from '../../components/dashboard/dashboard.component';
import { ForgotPasswordComponent } from '../../components/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from '../../components/verify-email/verify-email.component';
import { overviewComponent } from '../../components/dashboard/home/overview/overview.component';
import { resourcesComponent } from '../../components/dashboard/home/resources/resources.component';
import { facilitiesComponent } from '../../components/dashboard/home/facilities/facilities.component';

import { AuthGuard } from "../../shared/guard/auth.guard";

const routes: Routes = [
  { path: '', redirectTo: '/sign-in', pathMatch: 'full'},
  { path: 'sign-in',
   component: SignInComponent
  },
  { path: 'register-user', component: SignUpComponent},
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard],

  children:[
  
   {
       path: 'overview',
       component: overviewComponent
    },
    {
       path: 'resources',
       component: resourcesComponent
    },
    {
       path: 'facilities',
       component: facilitiesComponent
    },
    {
       path: 'research',
       component: overviewComponent
    },
    {
       path: 'fleet',
       component: overviewComponent
    },
    {
       path: 'defenses',
       component: overviewComponent
    },
    {
       path: 'universe',
       component: overviewComponent
    },
    {
      path: '',
      component: overviewComponent
   }
]
 

},
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'verify-email-address', component: VerifyEmailComponent }



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
